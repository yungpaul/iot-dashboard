/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as React from "react";
import {Component} from "react";
import {connect} from "react-redux";
import {IConfigState} from "./config";
import {State} from "./appState";

import * as Import from './dashboard/import';

import DashboardHost from "./dashboardHost";

import * as $ from "jquery";

export interface UsersProps {
    config: IConfigState
}

export interface UsersState {
    userRows: Array<any>;
    ownerName: string;
}

export class PageUsers extends Component<UsersProps, UsersState> {
    private apiEndpoint: string;

    constructor(props: UsersProps) {
        super(props);
        this.apiEndpoint = props.config.apiEndpoint;

        this.state = {userRows: new Array(), ownerName: null};

        this.state.userRows.push(this.createLoadingRow());
        this.updateUsers();
    }

    createLoadingRow() {
        return <tr>
            <th scope="row" data-label="UserId">
                <div className="slds-truncate" title="User Id">Loading...</div>
            </th>
            <td data-label="Account Name">
            </td>
            <td data-label="Close Date">
            </td>
        </tr>;
    }

    createRow(userId: string, dashboardName: string): any {
        return <tr>
            <th scope="row" data-label="UserId">
                <div className="slds-truncate" title="User Id">{userId}</div>
            </th>
            <td data-label="Account Name">
                <div className="slds-truncate" title="Dashboard"><a onClick={
                    (dispatch: any) => {
                        this.state.ownerName = userId;

                    }
                }>View Dashboard</a></div>
            </td>
            <td data-label="Close Date">
                <div className="slds-truncate" title="Ok">Ok</div>
            </td>
        </tr>;
    }

    updateUsers() {

        let this_: PageUsers = this;
        $.getJSON(this.apiEndpoint + "/home", result => {
            while (this_.state.userRows.length) {this_.state.userRows.pop();}
            result.forEach((e: string) => {
                this_.state.userRows.push(this.createRow(e, ""));
            });
            this_.forceUpdate();
        });

    }

    loadDashboard(userId: string, callback: any) {
        $.getJSON(this.apiEndpoint + "/dashboard/" + userId, result => {
            callback(JSON.stringify(result));
        });
    }

    render() {

        let userTable = <div className="slds-grid--vertical slds-grid_align-space slds-grid_pull-padded-medium">
            <div className="slds-col">
                <div className="slds-p-top_x-large"></div>
                <h1 className="slds-page-header__title slds-truncate slds-align-middle">System Users</h1>
                <div className="slds-p-top_large"></div>
            </div>
            <div className="slds-col">
                <table className="slds-table slds-table_striped slds-table_bordered slds-table_cell-buffer">
                    <thead>
                        <tr className="slds-text-title_caps">
                            <th scope="col">
                                <div className="slds-truncate" title="Account Id">Account Id</div>
                            </th>
                            <th scope="col">
                                <div className="slds-truncate" title="Dashboard">Dashboard</div>
                            </th>
                            <th scope="col">
                                <div className="slds-truncate" title="Status">Status</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.userRows}
                    </tbody>
                </table>

            </div>
        </div>;

        let userView = <div className="slds-grid--vertical slds-grid_align-space slds-grid_pull-padded-medium">
            <div className="slds-col">
                <nav role="navigation" aria-label="Breadcrumbs">
                    <ol className="slds-breadcrumb slds-list_horizontal">
                        <li className="slds-breadcrumb__item slds-text-title_caps"><a href="javascript:void(0);" onClick={() => {this.state.ownerName = null;}}>Users</a></li>
                        <li className="slds-breadcrumb__item slds-text-title_caps"><a href="javascript:void(0);">{this.state.ownerName}</a></li>
                    </ol>
                </nav>
            </div>
            <div className="slds-col">
                <DashboardHost owner={this.state.ownerName} userTailored={true} />
            </div>
        </div>;

        return this.state.ownerName == null ? userTable : userView;
    }
}

export default connect(
    (state: State) => {
        return {
            config: state.config,

        };
    }
)(PageUsers);