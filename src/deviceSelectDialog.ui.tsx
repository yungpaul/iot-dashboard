/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as React from 'react'
import {Component, KeyboardEvent} from "react";
import ModalDialog from "./modal/modalDialog.ui";
import * as WidgetPlugins from "./widgets/widgetPlugins";
import * as WidgetConfig from "./widgets/widgetConfig";
import {connect} from "react-redux";
import SettingsForm from "./ui/settingsForm.ui";
import * as ModalIds from './modal/modalDialogIds'
import {PropTypes as Prop} from "react";
import {IConfigState} from "./config";
import {State} from "./appState";

const DIALOG_ID = ModalIds.DEVICESELECT;
const FORM_ID = "device-select-form";

import * as $ from "jquery";

export interface DeviceSelectProp {
    config: IConfigState
    resetForm(formId: any): any
    owner: string
};

export interface DeviceSelectState {
}

export class DeviceSelectModal extends Component<DeviceSelectProp, DeviceSelectState> {
    private deviceData: any;
    private availableDeviceIds: Array<any> = new Array<any>();

    constructor(props: DeviceSelectProp) {
        super(props);

        this.getUserDevices((result: any) => {
            this.deviceData = result;
            
            this.updateAvailableDeviceIds(this.getDeviceTypes()[0]);
            this.forceUpdate()
        });
    }

    onSubmit(formData: any, dispatch: any) {
        //dispatch(WidgetConfig.createOrUpdateWidget(this.props.widgetId, this.props.widgetType, formData));
        return true;
    }

    getUserDevices(callback: any): void {
        $.getJSON(this.props.config.apiEndpoint + "/home/" + this.props.owner, (data: any) => {

            let devices: any = {};
            data.forEach((device: any) => {
                device.typesSensor.forEach((typeName: string) => {

                    if (devices[typeName] === undefined) {
                        devices[typeName] = new Array<String>();
                    }

                    devices[typeName].push(device.id);
                });
            });

            callback(devices);

        });
        return null;//let devices : Map<String, String> = new Map<String, String>();
    }

    getDeviceTypes(): Array<string> {
        let result: Array<string> = new Array<string>();

        if (this.deviceData === undefined) {
            return result;
        }

        for (var property in this.deviceData) {
            if (this.deviceData.hasOwnProperty(property)) {
                result.push(property);
            }
        }

        return result;
    }

    deviceTypeChanged() {
        let selectType: any = this.refs;

        this.updateAvailableDeviceIds(selectType.selectType.value);
    }

    updateAvailableDeviceIds(typeName: string) {

        for (var i = this.availableDeviceIds.length; i > 0; i--) {
            this.availableDeviceIds.pop();
        }

        this.deviceData[typeName].forEach((e: string) => {
            this.availableDeviceIds.push(<option>{e}</option>);
        });

        this.forceUpdate();
    }

    resetForm() {
        this.props.resetForm(FORM_ID);
    }

    render() {

        if (this.deviceData === undefined) {
            return <div>Loading...</div>
        }

        const props = this.props;
        const actions = [
            {
                className: "ui right button",
                label: "Create",
                onClick: () => {
                    this.resetForm();
                    return false;
                }
            },
            {
                className: "ui right red button",
                label: "Cancel",
                onClick: () => {
                    return true;
                }
            }
        ];

        let sensorTypes = new Array<any>()

        this.getDeviceTypes().forEach((typeName) => {
            sensorTypes.push(<option>{typeName}</option>);
        });

        return <ModalDialog id={DIALOG_ID}
            title={"Add Device"}
            actions={actions}
        >
            <div className="ui one column grid" >
                <div className="column" >
                    <div className="slds-form-element">
                        <label className="slds-form-element__label">Device Sensor Type</label>
                        <div className="slds-form-element__control">
                            <div className="slds-select_container">
                                <select onChange={this.deviceTypeChanged.bind(this)} className="slds-select" ref="selectType">
                                    {sensorTypes}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div className="slds-form-element">
                        <label className="slds-form-element__label">Device Id</label>
                        <div className="slds-form-element__control">
                            <div className="slds-select_container">
                                <select onChange={this.deviceTypeChanged.bind(this)} className="slds-select" ref="deviceid">
                                    {this.availableDeviceIds}
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ModalDialog>;
    }
}

export default connect(
    (state: State, ownProps: any) => {
        return {
            config: state.config,
            owner: ownProps.owner
        }
    },
    (dispatch: any) => {
        return {
            resetForm: (id: any) => {/*dispatch()*/}
        }
    }
)(DeviceSelectModal);
