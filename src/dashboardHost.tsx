/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as React from "react";
import {Component, KeyboardEvent} from "react";
import * as ReactDOM from "react-dom";
import {connect} from "react-redux";
import * as Global from "./dashboard/global.js";
import WidgetGrid from "./widgets/widgetGrid.ui.js";
import LayoutsNavItem from "./layouts/layouts.ui.js";
import WidgetConfigDialog from "./widgets/widgetConfigDialog.ui.js";
import DeviceSelectDialog from "./deviceSelectDialog.ui";
import DashboardMenuEntry from "./dashboard/dashboardMenuEntry.ui.js";
import ImportExportDialog from "./dashboard/importExportDialog.ui.js";
import DatasourceConfigDialog from "./datasource/datasourceConfigDialog.ui.js";
import DatasourceNavItem from "./datasource/datasourceNavItem.ui.js";
import WidgetsNavItem from "./widgets/widgetsNavItem.ui.js";
import PluginNavItem from "./pluginApi/pluginNavItem.ui";
import PluginsDialog from "./pluginApi/pluginsDialog.ui";
import * as Persistence from "./persistence";
import {IConfigState} from "./config";
import {State} from "./appState";
import DatasourceFrames from "./datasource/datasourceFrames.ui";

import * as ModalIds from "./modal/modalDialogIds";
import * as Modal from "./modal/modalDialog.js";

import * as Import from './dashboard/import';

import * as ui from './ui/elements.ui'

import * as $ from "jquery";

export interface HostProps {
    config: IConfigState
    owner: string,
    userTailored: boolean
    state: any,
    doImport: any,
    showDeviceSelect(): any
}

export interface HostState {
}

export class DashboardHost extends Component<HostProps, HostState> {

    constructor(props: HostProps) {
        super(props);
        this.state = {};

        this.loadDashboard((dashboard: any) => this.props.doImport(dashboard));
    }

    saveDashboard() {
        let dashboardJson = Import.serialize(this.props.state);

        $.ajax({
            url: this.props.config.apiEndpoint + "/dashboard/" + this.props.owner,
            type: "PUT",
            contentType: "application/json",
            data: dashboardJson
        });
    }

    loadDashboard(callback: any) {
        $.getJSON(this.props.config.apiEndpoint + "/dashboard/" + this.props.owner, result => {
            callback(JSON.stringify(result));
        });
    }

    render() {
        let addDeviceMenuOption = <li
            className="slds-context-bar__item slds-context-bar__dropdown-trigger slds-dropdown-trigger slds-dropdown-trigger--hover"
            aria-haspopup="true">
            <a href="javascript:void(0);" className="slds-context-bar__label-action" title="Add Device">
                <span className="slds-truncate" onClick={this.props.showDeviceSelect.bind(this)}>Add Device</span>
            </a>
        </li>;
        let result = <div className="slds-grid--vertical slds-grid_align-space slds-grid_pull-padded-medium">

            {(this.props.userTailored ? <DeviceSelectDialog owner={this.props.owner} /> : undefined)}
            <div className="slds-col">
                <div className="slds-context-bar">
                    <div className="slds-context-bar__primary">
                        <div className="slds-context-bar__item slds-context-bar__dropdown-trigger slds-dropdown-trigger slds-dropdown-trigger_click slds-no-hover">
                            <span className="slds-context-bar__label-action slds-context-bar__app-name">
                                <span className="slds-truncate" title="App Name">Dashboard: {this.props.owner}</span>
                            </span>
                        </div>
                    </div>
                    <WidgetsNavItem />
                    <DatasourceNavItem />

                    {(this.props.userTailored ? addDeviceMenuOption : undefined)}

                    <li
                        className="slds-context-bar__item slds-context-bar__dropdown-trigger slds-dropdown-trigger slds-dropdown-trigger--hover"
                        aria-haspopup="true">
                        <a href="javascript:void(0);" className="slds-context-bar__label-action" title="Layouts">
                            <span className="slds-truncate" onClick={() => {this.saveDashboard()}}>Save</span>
                        </a>
                    </li>
                </div>
            </div>
            <div className="slds-col">
                <WidgetGrid />
            </div>
        </div>;

        return result;
    }
}

export default connect(
    (state: State, ownProps: any) => {
        return {
            config: state.config,
            owner: ownProps.owner,
            state: state,
            userTailored: ownProps.userTailored === undefined ? false : ownProps.userTailored,
        };
    },
    (dispatch: any) => {
        return {
            doImport: (state: any) => {dispatch(Import.doImport(state));},

            showDeviceSelect: (state: any) => {
                dispatch(Modal.showModal(ModalIds.DEVICESELECT, {datasource: state}));
            }
        };
    }
)(DashboardHost);