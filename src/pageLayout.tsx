/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as React from "react";
import {Component, KeyboardEvent} from "react";
import * as ReactDOM from "react-dom";
import {connect} from "react-redux";
import * as Global from "./dashboard/global.js";
import WidgetGrid from "./widgets/widgetGrid.ui.js";
import LayoutsNavItem from "./layouts/layouts.ui.js";
import WidgetConfigDialog from "./widgets/widgetConfigDialog.ui.js";
import DashboardMenuEntry from "./dashboard/dashboardMenuEntry.ui.js";
import ImportExportDialog from "./dashboard/importExportDialog.ui.js";
import DatasourceConfigDialog from "./datasource/datasourceConfigDialog.ui.js";
import DatasourceNavItem from "./datasource/datasourceNavItem.ui.js";
import WidgetsNavItem from "./widgets/widgetsNavItem.ui.js";
import PluginNavItem from "./pluginApi/pluginNavItem.ui";
import PluginsDialog from "./pluginApi/pluginsDialog.ui";
import * as Persistence from "./persistence";
import {IConfigState} from "./config";
import {State} from "./appState";
import DatasourceFrames from "./datasource/datasourceFrames.ui";
import PageUsers from "./pageUsers";
import PageLogin from "./pageLogin";
import DashboardHost from "./dashboardHost";

declare var gapi: any;

interface LayoutProps {
    setReadOnly(readOnly: boolean): void
    isReadOnly: boolean
    devMode: boolean
    config: IConfigState
}

interface LayoutState {
    hover: boolean;
}

export class Layout extends Component<LayoutProps, LayoutState> {
    private displayDocument: any;
    private activeIndex: any;

    constructor(props: LayoutProps) {
        super(props);
        this.state = {hover: false};

        this.gotoOverview();
    }

    onReadOnlyModeKeyPress(e: KeyboardEvent<any>) {
        //console.log("key pressed", event.keyCode);
        const intKey = (window.event) ? e.which : e.keyCode;
        if (intKey === 27) {
            this.props.setReadOnly(!this.props.isReadOnly);
        }
    }

    gotoOverview() {
        this.activeIndex = 0;
        this.displayDocument = <DashboardHost owner="overview" />;
    }

    gotoUsers() {
        this.activeIndex = 1;
        this.displayDocument = <PageUsers />;
    }

    gotoSettings() {
        this.activeIndex = 2;
        this.displayDocument = <div>This is the settings page...</div>;
    }

    logout() {
        gapi.auth2.getAuthInstance().signOut();
    }

    componentDidMount() {
        if (this.props.devMode) {
            this.onReadOnlyModeKeyPress = this.onReadOnlyModeKeyPress.bind(this);

            ReactDOM.findDOMNode<any>(this)
                .offsetParent
                .addEventListener('keydown', this.onReadOnlyModeKeyPress);
        }
    }

    render() {
        const props = this.props;
        const devMode = props.devMode;
        const showMenu = props.devMode && (!props.isReadOnly || this.state.hover);

        let result = <div className="slds-grid slds-grid--frame" onKeyUp={(event) => this.onReadOnlyModeKeyPress(event)}>
            <div>
                <WidgetConfigDialog />
                <ImportExportDialog />
                <DatasourceConfigDialog />
                <PluginsDialog />
            </div>
            <div className="slds-col-2">
                <nav className="slds-nav-vertical navMenu" aria-label="Sub page">
                    <div className="slds-nav-vertical__section">
                        <h2 id="entity-header" className="slds-nav-vertical__title slds-text-title_caps">IoT - Board</h2>
                        <ul>
                            <li className={(this.activeIndex == 0 ? "slds-nav-vertical__item slds-is-active" : "slds-nav-vertical__item")}><a onClick={this.gotoOverview.bind(this)} className="slds-nav-vertical__action" aria-describedby="entity-header" aria-current="page">Overview</a></li>
                            <li className={(this.activeIndex == 1 ? "slds-nav-vertical__item slds-is-active" : "slds-nav-vertical__item")}><a onClick={this.gotoUsers.bind(this)} className="slds-nav-vertical__action" aria-describedby="entity-header">User</a></li>
                            <li className={(this.activeIndex == 2 ? "slds-nav-vertical__item slds-is-active" : "slds-nav-vertical__item")}><a onClick={this.gotoSettings.bind(this)} className="slds-nav-vertical__action" aria-describedby="entity-header">Settings</a></li>
                            <li className="slds-nav-vertical__item"><a className="slds-nav-vertical__action" aria-describedby="entity-header" onClick={this.logout.bind(this)}>Logout</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div className="slds-col">
                {this.displayDocument}
            </div>

            <DatasourceFrames />
        </div>;


        return result;
    }

}

export default connect(
    (state: State) => {
        return {
            isReadOnly: state.global.isReadOnly,
            devMode: state.config.devMode,
            config: state.config
        };
    },
    (dispatch: any) => {
        return {
            setReadOnly: (isReadOnly: boolean) => dispatch(Global.setReadOnly(isReadOnly))
        };
    }
)(Layout);
