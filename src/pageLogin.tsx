/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import * as React from "react";
import {Component} from "react";
import {connect} from "react-redux";
import {IConfigState} from "./config";
import {State} from "./appState";

import * as Import from './dashboard/import';

import DashboardHost from "./dashboardHost";

import * as $ from "jquery";

import Layout from "./pageLayout"

const logo = require("./img/logo.png");

declare var gapi: any;

export interface UsersProps {
    config: IConfigState
}

export interface UsersState {
    isAuthenticated: boolean;
}

export class PageLogin extends Component<UsersProps, UsersState> {
    private apiEndpoint: string;

    constructor(props: UsersProps) {
        super(props);
        this.apiEndpoint = props.config.apiEndpoint;

        this.state = {isAuthenticated: false};

    }

    componentDidMount() {
    }

    renderGoogleLogin() {

        gapi.signin2.render('my-signin2', {
            'width': 200,
            'height': 40,
            'longtitle': true,
            'theme': 'light',
            'onsuccess': () => {
                gapi.auth2.getAuthInstance().isSignedIn.listen((signedIn: boolean) => {
                    this.state.isAuthenticated = signedIn;
                    this.forceUpdate();
                });
                this.state.isAuthenticated = true; this.forceUpdate()
            },
            'onfailure': (err: any) => {throw err}
        });
    }

    render() {

        let loginPage = <div className="slds-grid slds-grid_align-center slds-grid_vertical-align-center">
            <div className="slds-col">
            </div>
            <div className="slds-col">
                <div onLoad={this.renderGoogleLogin.bind(this)} className="slds-grid slds-grid--vertical slds-grid_align-center">
                    <div className="slds-col">
                        <div className="slds-page-header">
                            <div className="slds-media">
                                <div className="slds-media__body">
                                    <h1 className="slds-page-header__title slds-truncate slds-align-middle">Plasmatic Dashboard - Authentication</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="slds-col">
                        <div className="slds-col">
                            <div className="slds-align_absolute-center"><img src={String(logo)} /></div>
                        </div>

                        <div className="slds-col">
                            <div className="slds-text-align_center">Please authenticate by pressing the login button below to proceed to the IoT - Dashboard</div>
                        </div>

                        <div className="slds-p-top_xx-large"></div>
                        <div className="slds-col">
                            <div className="slds-align_absolute-center"><div id="my-signin2"></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="slds-col">
            </div>
        </div>;

        if (this.state.isAuthenticated) {
            return <Layout />
        } else {
            return loginPage;
        }
    }
}

export default connect(
    (state: State) => {
        return {
            config: state.config,

        };
    }
)(PageLogin);